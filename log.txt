[33m
[1m[33mWarning: [0m[0m[1mopenstack_networking_router_v2.example_router_1: "external_gateway": [DEPRECATED] use external_network_id instead[0m

[0m[0m[0m

[0m[1mopenstack_networking_floatingip_v2.example_floatip_manager: Refreshing state... (ID: 576c9232-e29e-446e-870a-a1ce53548f83)[0m
[0m[1mopenstack_networking_network_v2.example_network1: Refreshing state... (ID: abccd100-359c-47df-b4c6-19c5bddd8ec4)[0m
[0m[1mopenstack_compute_keypair_v2.test-keypair: Refreshing state... (ID: ukcloudos)[0m
[0m[1mopenstack_compute_secgroup_v2.example_secgroup_1: Refreshing state... (ID: 229d1181-7043-4ece-b6ff-b155a33d14eb)[0m
[0m[1mopenstack_networking_subnet_v2.example_subnet1: Refreshing state... (ID: 8095e3f6-0b61-468e-a6c6-d97127fde830)[0m

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  [32m+[0m create
 [36m<=[0m read (data resources)
[0m
Terraform will perform the following actions:

[36m [36m<=[0m [36mdata.template_file.managerinit
[0m      id:                         <computed>
      rendered:                   <computed>
      template:                   "#!/bin/bash\n# Script that will run at first boot via Openstack\n# using user_data via cloud-init.\n\n# Copy Tokens from master1 => masterX\nsudo scp -o StrictHostKeyChecking=no -o NoHostAuthenticationForLocalhost=yes -o UserKnownHostsFile=/dev/null -i /home/core/.ssh/key.pem core@${swarm_manager}:/home/core/manager-token /home/core/manager-token\n\n# Copy docker-compose.yml file\nsudo scp -o StrictHostKeyChecking=no -o NoHostAuthenticationForLocalhost=yes -o UserKnownHostsFile=/dev/null -i /home/core/.ssh/key.pem core@${swarm_manager}:/home/core/docker-compose.yml /home/core/docker-compose.yml\nsudo docker swarm join --token $(cat /home/core/manager-token) ${swarm_manager}"
      vars.%:                     <computed>
[0m
[0m[36m [36m<=[0m [36mdata.template_file.slaveinit
[0m      id:                         <computed>
      rendered:                   <computed>
      template:                   "#!/bin/bash\n# Script that will run at first boot via Openstack\n# using user_data via cloud-init.\n\n\nsudo scp -o StrictHostKeyChecking=no -o NoHostAuthenticationForLocalhost=yes -o UserKnownHostsFile=/dev/null -i /home/core/.ssh/key.pem core@${swarm_manager}:/home/core/worker-token /home/core/worker-token\nsudo docker swarm join --token $(cat /home/core/worker-token) ${swarm_manager}\n\n# Horrible hack, as Swarm doesn't evenly distribute to new nodes \n# https://github.com/docker/docker/issues/24103\n#ssh -o StrictHostKeyChecking=no -o NoHostAuthenticationForLocalhost=yes -o UserKnownHostsFile=/dev/null  -i /home/core/.ssh/key.pem core@${swarm_manager} \"docker service scale mystack_php-fpm=3\"\n#ssh -o StrictHostKeyChecking=no -o NoHostAuthenticationForLocalhost=yes -o UserKnownHostsFile=/dev/null  -i /home/core/.ssh/key.pem core@${swarm_manager} \"docker service scale mystacK_web=3\"\n\n# Scale to the number of instances we should have once the script has finished.\n# This means it may scale to 50 even though we only have 10, with 40 still processing.\n# Hence the issue above.\n#ssh -o StrictHostKeyChecking=no -o NoHostAuthenticationForLocalhost=yes -o UserKnownHostsFile=/dev/null  -i /home/core/.ssh/key.pem core@${swarm_manager} \"docker service scale mystack_php-fpm=${node_count}\"\n#ssh -o StrictHostKeyChecking=no -o NoHostAuthenticationForLocalhost=yes -o UserKnownHostsFile=/dev/null  -i /home/core/.ssh/key.pem core@${swarm_manager} \"docker service scale mystack_web=${node_count}\"\n#ssh -o StrictHostKeyChecking=no -o NoHostAuthenticationForLocalhost=yes -o UserKnownHostsFile=/dev/null  -i /home/core/.ssh/key.pem core@${swarm_manager} \"docker service scale mystack_collectd=${node_count}\"\n\n## Forces redistribution across all nodes\n#ssh -o StrictHostKeyChecking=no -o NoHostAuthenticationForLocalhost=yes -o UserKnownHostsFile=/dev/null  -i /home/core/.ssh/key.pem core@${swarm_manager} \"docker service update --force mystack_php-fpm\"\n#ssh -o StrictHostKeyChecking=no -o NoHostAuthenticationForLocalhost=yes -o UserKnownHostsFile=/dev/null  -i /home/core/.ssh/key.pem core@${swarm_manager} \"docker service update --force mystack_web\"\n#ssh -o StrictHostKeyChecking=no -o NoHostAuthenticationForLocalhost=yes -o UserKnownHostsFile=/dev/null  -i /home/core/.ssh/key.pem core@${swarm_manager} \"docker service update --force mystack_collectd\""
      vars.%:                     <computed>
[0m
[0m[32m  [32m+[0m [32mopenstack_compute_floatingip_associate_v2.fip_1
[0m      id:                         <computed>
      floating_ip:                "51.179.210.208"
      instance_id:                "${openstack_compute_instance_v2.swarm_manager.id}"
      region:                     <computed>
[0m
[0m[32m  [32m+[0m [32mopenstack_compute_instance_v2.swarm_manager
[0m      id:                         <computed>
      access_ip_v4:               <computed>
      access_ip_v6:               <computed>
      all_metadata.%:             <computed>
      availability_zone:          <computed>
      flavor_id:                  "8dd6b3c2-fd5f-434a-a3e9-3b638a27859e"
      flavor_name:                <computed>
      force_delete:               "false"
      image_id:                   "9804b597-4b13-41b5-a77d-2fc6d798d4ac"
      image_name:                 <computed>
      key_pair:                   "ukcloudos"
      name:                       "swarm_manager_0"
      network.#:                  "1"
      network.0.access_network:   "false"
      network.0.fixed_ip_v4:      <computed>
      network.0.fixed_ip_v6:      <computed>
      network.0.floating_ip:      <computed>
      network.0.mac:              <computed>
      network.0.name:             "example_network_1"
      network.0.port:             <computed>
      network.0.uuid:             <computed>
      region:                     <computed>
      security_groups.#:          "1"
      security_groups.3365680318: "example_secgroup_1"
      stop_before_destroy:        "false"
[0m
[0m[32m  [32m+[0m [32mopenstack_compute_instance_v2.swarm_managerx[0]
[0m      id:                         <computed>
      access_ip_v4:               <computed>
      access_ip_v6:               <computed>
      all_metadata.%:             <computed>
      availability_zone:          <computed>
      flavor_id:                  "8dd6b3c2-fd5f-434a-a3e9-3b638a27859e"
      flavor_name:                <computed>
      force_delete:               "false"
      image_id:                   "9804b597-4b13-41b5-a77d-2fc6d798d4ac"
      image_name:                 <computed>
      key_pair:                   "ukcloudos"
      name:                       "swarm_manager_1"
      network.#:                  "1"
      network.0.access_network:   "false"
      network.0.fixed_ip_v4:      <computed>
      network.0.fixed_ip_v6:      <computed>
      network.0.floating_ip:      <computed>
      network.0.mac:              <computed>
      network.0.name:             "example_network_1"
      network.0.port:             <computed>
      network.0.uuid:             <computed>
      region:                     <computed>
      security_groups.#:          "1"
      security_groups.3365680318: "example_secgroup_1"
      stop_before_destroy:        "false"
      user_data:                  "abc05f33818f594b9c09e0d3d2182d11e2af8d11"
[0m
[0m[32m  [32m+[0m [32mopenstack_compute_instance_v2.swarm_managerx[1]
[0m      id:                         <computed>
      access_ip_v4:               <computed>
      access_ip_v6:               <computed>
      all_metadata.%:             <computed>
      availability_zone:          <computed>
      flavor_id:                  "8dd6b3c2-fd5f-434a-a3e9-3b638a27859e"
      flavor_name:                <computed>
      force_delete:               "false"
      image_id:                   "9804b597-4b13-41b5-a77d-2fc6d798d4ac"
      image_name:                 <computed>
      key_pair:                   "ukcloudos"
      name:                       "swarm_manager_2"
      network.#:                  "1"
      network.0.access_network:   "false"
      network.0.fixed_ip_v4:      <computed>
      network.0.fixed_ip_v6:      <computed>
      network.0.floating_ip:      <computed>
      network.0.mac:              <computed>
      network.0.name:             "example_network_1"
      network.0.port:             <computed>
      network.0.uuid:             <computed>
      region:                     <computed>
      security_groups.#:          "1"
      security_groups.3365680318: "example_secgroup_1"
      stop_before_destroy:        "false"
      user_data:                  "abc05f33818f594b9c09e0d3d2182d11e2af8d11"
[0m
[0m[32m  [32m+[0m [32mopenstack_compute_instance_v2.swarm_slave[0]
[0m      id:                         <computed>
      access_ip_v4:               <computed>
      access_ip_v6:               <computed>
      all_metadata.%:             <computed>
      availability_zone:          <computed>
      flavor_id:                  "c46be6d1-979d-4489-8ffe-e421a3c83fdd"
      flavor_name:                <computed>
      force_delete:               "false"
      image_id:                   "9804b597-4b13-41b5-a77d-2fc6d798d4ac"
      image_name:                 <computed>
      key_pair:                   "ukcloudos"
      name:                       "swarm_slave_0"
      network.#:                  "1"
      network.0.access_network:   "false"
      network.0.fixed_ip_v4:      <computed>
      network.0.fixed_ip_v6:      <computed>
      network.0.floating_ip:      <computed>
      network.0.mac:              <computed>
      network.0.name:             "example_network_1"
      network.0.port:             <computed>
      network.0.uuid:             <computed>
      region:                     <computed>
      security_groups.#:          "1"
      security_groups.3365680318: "example_secgroup_1"
      stop_before_destroy:        "false"
      user_data:                  "06cc70ad20cf32fbf70277cd7e3957674e98b867"
[0m
[0m[32m  [32m+[0m [32mopenstack_compute_instance_v2.swarm_slave[1]
[0m      id:                         <computed>
      access_ip_v4:               <computed>
      access_ip_v6:               <computed>
      all_metadata.%:             <computed>
      availability_zone:          <computed>
      flavor_id:                  "c46be6d1-979d-4489-8ffe-e421a3c83fdd"
      flavor_name:                <computed>
      force_delete:               "false"
      image_id:                   "9804b597-4b13-41b5-a77d-2fc6d798d4ac"
      image_name:                 <computed>
      key_pair:                   "ukcloudos"
      name:                       "swarm_slave_1"
      network.#:                  "1"
      network.0.access_network:   "false"
      network.0.fixed_ip_v4:      <computed>
      network.0.fixed_ip_v6:      <computed>
      network.0.floating_ip:      <computed>
      network.0.mac:              <computed>
      network.0.name:             "example_network_1"
      network.0.port:             <computed>
      network.0.uuid:             <computed>
      region:                     <computed>
      security_groups.#:          "1"
      security_groups.3365680318: "example_secgroup_1"
      stop_before_destroy:        "false"
      user_data:                  "06cc70ad20cf32fbf70277cd7e3957674e98b867"
[0m
[0m[32m  [32m+[0m [32mopenstack_compute_instance_v2.swarm_slave[2]
[0m      id:                         <computed>
      access_ip_v4:               <computed>
      access_ip_v6:               <computed>
      all_metadata.%:             <computed>
      availability_zone:          <computed>
      flavor_id:                  "c46be6d1-979d-4489-8ffe-e421a3c83fdd"
      flavor_name:                <computed>
      force_delete:               "false"
      image_id:                   "9804b597-4b13-41b5-a77d-2fc6d798d4ac"
      image_name:                 <computed>
      key_pair:                   "ukcloudos"
      name:                       "swarm_slave_2"
      network.#:                  "1"
      network.0.access_network:   "false"
      network.0.fixed_ip_v4:      <computed>
      network.0.fixed_ip_v6:      <computed>
      network.0.floating_ip:      <computed>
      network.0.mac:              <computed>
      network.0.name:             "example_network_1"
      network.0.port:             <computed>
      network.0.uuid:             <computed>
      region:                     <computed>
      security_groups.#:          "1"
      security_groups.3365680318: "example_secgroup_1"
      stop_before_destroy:        "false"
      user_data:                  "06cc70ad20cf32fbf70277cd7e3957674e98b867"
[0m
[0m[32m  [32m+[0m [32mopenstack_compute_instance_v2.swarm_slave[3]
[0m      id:                         <computed>
      access_ip_v4:               <computed>
      access_ip_v6:               <computed>
      all_metadata.%:             <computed>
      availability_zone:          <computed>
      flavor_id:                  "c46be6d1-979d-4489-8ffe-e421a3c83fdd"
      flavor_name:                <computed>
      force_delete:               "false"
      image_id:                   "9804b597-4b13-41b5-a77d-2fc6d798d4ac"
      image_name:                 <computed>
      key_pair:                   "ukcloudos"
      name:                       "swarm_slave_3"
      network.#:                  "1"
      network.0.access_network:   "false"
      network.0.fixed_ip_v4:      <computed>
      network.0.fixed_ip_v6:      <computed>
      network.0.floating_ip:      <computed>
      network.0.mac:              <computed>
      network.0.name:             "example_network_1"
      network.0.port:             <computed>
      network.0.uuid:             <computed>
      region:                     <computed>
      security_groups.#:          "1"
      security_groups.3365680318: "example_secgroup_1"
      stop_before_destroy:        "false"
      user_data:                  "06cc70ad20cf32fbf70277cd7e3957674e98b867"
[0m
[0m[32m  [32m+[0m [32mopenstack_compute_instance_v2.swarm_slave[4]
[0m      id:                         <computed>
      access_ip_v4:               <computed>
      access_ip_v6:               <computed>
      all_metadata.%:             <computed>
      availability_zone:          <computed>
      flavor_id:                  "c46be6d1-979d-4489-8ffe-e421a3c83fdd"
      flavor_name:                <computed>
      force_delete:               "false"
      image_id:                   "9804b597-4b13-41b5-a77d-2fc6d798d4ac"
      image_name:                 <computed>
      key_pair:                   "ukcloudos"
      name:                       "swarm_slave_4"
      network.#:                  "1"
      network.0.access_network:   "false"
      network.0.fixed_ip_v4:      <computed>
      network.0.fixed_ip_v6:      <computed>
      network.0.floating_ip:      <computed>
      network.0.mac:              <computed>
      network.0.name:             "example_network_1"
      network.0.port:             <computed>
      network.0.uuid:             <computed>
      region:                     <computed>
      security_groups.#:          "1"
      security_groups.3365680318: "example_secgroup_1"
      stop_before_destroy:        "false"
      user_data:                  "06cc70ad20cf32fbf70277cd7e3957674e98b867"
[0m
[0m[32m  [32m+[0m [32mopenstack_networking_router_interface_v2.example_router_interface_1
[0m      id:                         <computed>
      port_id:                    <computed>
      region:                     <computed>
      router_id:                  "${openstack_networking_router_v2.example_router_1.id}"
      subnet_id:                  "8095e3f6-0b61-468e-a6c6-d97127fde830"
[0m
[0m[32m  [32m+[0m [32mopenstack_networking_router_v2.example_router_1
[0m      id:                         <computed>
      admin_state_up:             <computed>
      availability_zone_hints.#:  <computed>
      distributed:                <computed>
      enable_snat:                <computed>
      external_fixed_ip.#:        <computed>
      external_gateway:           "893a5b59-081a-4e3a-ac50-1e54e262c3fa"
      external_network_id:        <computed>
      name:                       "example_router1"
      region:                     <computed>
      tenant_id:                  <computed>
[0m
[0m
[0m[1mPlan:[0m 11 to add, 0 to change, 0 to destroy.[0m

[0m[1mDo you want to perform these actions?[0m
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  [1mEnter a value:[0m [0m
[0m[1mopenstack_networking_router_v2.example_router_1: Creating...[0m
  admin_state_up:            "" => "<computed>"
  availability_zone_hints.#: "" => "<computed>"
  distributed:               "" => "<computed>"
  enable_snat:               "" => "<computed>"
  external_fixed_ip.#:       "" => "<computed>"
  external_gateway:          "" => "893a5b59-081a-4e3a-ac50-1e54e262c3fa"
  external_network_id:       "" => "<computed>"
  name:                      "" => "example_router1"
  region:                    "" => "<computed>"
  tenant_id:                 "" => "<computed>"[0m
[0m[1mopenstack_compute_instance_v2.swarm_manager: Creating...[0m
  access_ip_v4:               "" => "<computed>"
  access_ip_v6:               "" => "<computed>"
  all_metadata.%:             "" => "<computed>"
  availability_zone:          "" => "<computed>"
  flavor_id:                  "" => "8dd6b3c2-fd5f-434a-a3e9-3b638a27859e"
  flavor_name:                "" => "<computed>"
  force_delete:               "" => "false"
  image_id:                   "" => "9804b597-4b13-41b5-a77d-2fc6d798d4ac"
  image_name:                 "" => "<computed>"
  key_pair:                   "" => "ukcloudos"
  name:                       "" => "swarm_manager_0"
  network.#:                  "" => "1"
  network.0.access_network:   "" => "false"
  network.0.fixed_ip_v4:      "" => "<computed>"
  network.0.fixed_ip_v6:      "" => "<computed>"
  network.0.floating_ip:      "" => "<computed>"
  network.0.mac:              "" => "<computed>"
  network.0.name:             "" => "example_network_1"
  network.0.port:             "" => "<computed>"
  network.0.uuid:             "" => "<computed>"
  region:                     "" => "<computed>"
  security_groups.#:          "" => "1"
  security_groups.3365680318: "" => "example_secgroup_1"
  stop_before_destroy:        "" => "false"[0m
